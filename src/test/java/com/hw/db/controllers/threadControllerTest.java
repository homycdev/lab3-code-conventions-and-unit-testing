package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.controllers.threadController;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.stubbing.OngoingStubbing;
import org.mockito.stubbing.Stubbing;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class threadControllerTest {
    private Thread thread;
    private String slug = "slug";
    private Thread nullThread;
    private List<Post> postsList;
    private Vote vote;
    private User user;


    @BeforeEach
    @DisplayName("Before All: Thread config.")
    void setUp() {
        postsList = Collections.emptyList();
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());

        thread = new Thread(
                "testAuthor",
                timestamp,
                "testForum",
                "testMessage",
                slug,
                "testTitle",
                0);

        nullThread = new Thread(
                null,
                null,
                null,
                null,
                null,
                null,
                0
        );

        user = new User(
                "sample",
                "email",
                "fullname",
                "about"
        );

        vote = new Vote(
                "sample",
                1
        );
    }


    @Test
    @DisplayName("Create post: success")
    void createPost() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {

            threadDaoMock
                    .when(
                            () -> ThreadDAO.getThreadBySlug(slug)
                    )
                    .thenReturn(thread);
            threadController controller = new threadController();
            assertEquals(
                    ResponseEntity.status(HttpStatus.CREATED).body(postsList),
                    controller.createPost(slug, postsList),
                    "Posts successfully created"
            );
        }
    }

    @Test
    @DisplayName("Check id or slug: fail")
    void checkIdOrSlug() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDaoMock.when(
                    () -> ThreadDAO.getThreadById(1)
            ).thenReturn(thread);
            threadController controller = new threadController();
            assertNull(
                    controller.CheckIdOrSlug("01248"),
                    "Checking fake id"
            );
        }
    }

    @Test
    @DisplayName("Create Post: success")
    void posts() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDaoMock.when(
                    () -> ThreadDAO.getThreadBySlug("ezSlug")
            ).thenReturn(thread);
            threadController controller = new threadController();
            assertEquals(
                    ResponseEntity.status(HttpStatus.OK).body(postsList),
                    controller.Posts("ezSlug", 1, 0, "order", true),
                    "Success"
            );
        }
    }

    @Test
    @DisplayName("Create Post: throws NPE")
    void postNPE() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDaoMock.when(
                    () -> ThreadDAO.getThreadBySlug("ezSlug")
            ).thenReturn(thread);
            threadController controller = new threadController();
            assertThrows(
                    NullPointerException.class,
                    () -> Mockito
                            .when(controller.Posts("igr", 1, 0, "1", false))
                            .thenThrow(NullPointerException.class));
        }
    }

    @Test
    @DisplayName("Change: NPE")
    void change() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDaoMock.when(
                    () -> ThreadDAO.getThreadBySlug(slug)
            ).thenReturn(thread);
            threadController controller = new threadController();
            assertThrows(
                    NullPointerException.class,
                    () -> controller.change(slug, thread)
            );
        }
    }

    @Test
    @DisplayName("Info: success")
    void infoTest() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {

            threadDaoMock.when(
                    () -> ThreadDAO.getThreadById(0)
            ).thenReturn(thread);

            threadDaoMock.when(
                    () -> ThreadDAO.getThreadBySlug(slug)
            ).thenReturn(thread);

            threadController controller = new threadController();

            assertEquals(
                    ResponseEntity.status(HttpStatus.OK).body(thread),
                    controller.info(slug),
                    "Info about given slug"
            );

            assertEquals(
                    thread,
                    ThreadDAO.getThreadBySlug(slug)
            );
        }
    }

    @Test
    @DisplayName("Create Vote: Success")
    void createVote() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {

                threadDaoMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                userMock.when(() -> UserDAO.Info("sample")).thenReturn(user);

                threadController controller = new threadController();
                assertEquals(
                        ResponseEntity.status(HttpStatus.OK).body(thread),
                        controller.createVote(slug, vote),
                        "Vote created");
            }
        }
    }
}
